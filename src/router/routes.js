
export default [
  {
    path: '/',
    component: () => import('layouts/index_layout'),
    children: [
      { path: '', component: () => import('pages/index') }
    ]
  },
  {
    path: '/:school_url',
    component: () => import('layouts/layout_user'),
    children: [
      { path: '', component: () => import('pages/index_school') },
      { path: ':user_id', component: () => import('pages/index_user') }
    ]
  },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
