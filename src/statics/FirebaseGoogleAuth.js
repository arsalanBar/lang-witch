import firebase from 'firebase'


var provider = new fireApp.auth.GoogleAuthProvider()

fireApp.auth().languageCode = 'ru'
// To apply the default browser preference instead of explicitly setting it.
// firebase.auth().useDeviceLanguage()

provider.setCustomParameters({
  'login_hint': 'user@example.com'
})

// var res1, error1

export let GoogleAuth = fireApp.auth().signInWithPopup(provider).then(function (result) {
  // This gives you a Google Access Token. You can use it to access the Google API.
  // var token = result.credential.accessToken;
  // The signed-in user info.
  // var user = result.user;
  // res1 = result

  console.log(result)
  // ...
}).catch(function (error) {
  console.log(error)
  // Handle Errors here.
  // var errorCode = error.code;
  // var errorMessage = error.message;
  // The email of the user's account used.
  // var email = error.email;
  // The firebase.auth.AuthCredential type that was used.
  // var credential = error.credential;
  // error1 = error
})

// export const res = res1

// export const error = error1
