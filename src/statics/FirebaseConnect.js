import firebase from 'FireBase'

const config = {
  // Initialize Firebase
  apiKey: 'AIzaSyCBZtrsAg6uyIwZpzIdfJu5qyIIPIuu7L0',
  authDomain: 'lang-witch.firebaseapp.com',
  databaseURL: 'https://lang-witch.firebaseio.com',
  projectId: 'lang-witch',
  storageBucket: 'lang-witch.appspot.com',
  messagingSenderId: '816722773475'
}

const fireApp = firebase.initializeApp(config)

export const fba = fireApp

export const db = fireApp.database()

export const namesRef = db.ref()
